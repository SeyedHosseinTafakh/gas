from DB import *
import os
import werkzeug
from flask import Flask , jsonify
from flask_restful import Api, Resource, reqparse
import random

class zemanat_nameha(Resource):
    def get(self):
        # parser = reqparse.RequestParser()
        # parser.add_argument('id')
        # args = parser.parse_args()
        mycursor.execute('select * from zemanat_name')
        return mycursor.fetchall()
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('bank_name',required=True)
        parser.add_argument('zemanat_name_code',required=True)
        parser.add_argument('tarikh_sar_resid',required=True)
        parser.add_argument('nerkh_tasri',required=True)
        parser.add_argument('taaghirat_dar_pishraft_fiziki',required=True)
        parser.add_argument('mablaghe_dolar_zemanatName',required=True)
        parser.add_argument('moadele_riyali_zemanat_name',required=True)
        parser.add_argument('tarikh_berooz_resani',required=True)
        parser.add_argument("peyvast", type=werkzeug.datastructures.FileStorage, location='files')
        args = parser.parse_args()
        file = args['peyvast']
        if file:
            dirname = os.path.dirname(__file__)
            fileName = str(random.randint(1,9999)*5)+file.filename
            file.save(os.path.join(dirname,'../files',fileName))
            # fileName = file.filename
        else:
            fileName = None
        query = 'insert into zemanat_name (bank_name,zemanat_name_code , tarikh_sar_resid ,nerkh_tasri,taaghirat_dar_pishraft_fiziki , mablaghe_dolar_zemanatName,moadele_riyali_zemanat_name ,tarikh_berooz_resani,peyvast) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)'
        values = (args['bank_name'],args['zemanat_name_code'],args['tarikh_sar_resid'],args['nerkh_tasri'],args['taaghirat_dar_pishraft_fiziki'],args['mablaghe_dolar_zemanatName'],args['moadele_riyali_zemanat_name'],args['tarikh_berooz_resani'],fileName)
        mycursor.execute(query, values)
        mydb.commit()
        return True
    def delete(self):
        mydb.commit()
        parser = reqparse.RequestParser()
        parser.add_argument('id',required=True)
        args = parser.parse_args()
        mycursor.execute('SELECT * from zemanat_name where id = %s',(args['id'],))
        zemanat_name = mycursor.fetchall()
        if len(zemanat_name) <= 0:
            return False
        dirname = os.path.dirname(__file__)
        os.remove(os.path.join(dirname,'../files/'+zemanat_name[0][9]))
        mycursor.execute('DELETE from  zemanat_name where id = %s',(args['id'],))
        mydb.commit()
        return True


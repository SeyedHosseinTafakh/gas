from DB import *
import os
import werkzeug
from flask import Flask , jsonify
from flask_restful import Api, Resource, reqparse
import pandas
import json
from timeFunctions import *
from dateutil.relativedelta import *
from datetime import timedelta
from khayyam import JalaliDate


class ghest_bandi_kole_gostare_ha_tajamoee(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('time',required=True)
        args = parser.parse_args()
        retSum = {}
        n = 1
        while n < 11:
            mycursor.execute("select * from gostare where id = %s ",(n,))
            gostare_data = mycursor.fetchall()
            mydb.commit()
            query = "select * from gostare_pishraft where gostare_id = %s and malg =1 and tarikh < %s"
            values = (n ,args['time'])
            mycursor.execute(query , values)
            gostare_pishrafts = mycursor.fetchall()
            df = self.opencsv()
            column = df.loc[0:29 , str(gostare_data[0][0])]
            ret = {}
            i = 0
            for gostare_pishraft in gostare_pishrafts:
                ret[gostare_pishraft[5]] = {}

                d = gostare_pishraft[3].split('-')
                time = JalaliDate(d[0], int(d[1]), d[2])
                month = int(d[1])+2
                # x = float(gostare_pishraft[2])/float(gostare_data[0][2]) * float(column[i])
                while i < 29:
                    if month > 12 :
                        month = 1
                        d[0] = str(int(d[0])+1)
                    column[i]= column[i].replace(',', '')
                    ret[gostare_pishraft[5]][i] = [
                        float(gostare_pishraft[2])/float(gostare_data[0][2]) * float(column[i])/100,
                        str(JalaliDate(d[0], str(month) , d[2]))
                    ]
                    i +=1
                    month +=1

                i = 0

            recent_made_sum = self.makeDataFrame(ret,args['time'])
            # print(recent_made_sum)
            sum_jadid = 0
            if not recent_made_sum.empty:
                sum_x = recent_made_sum.loc[:args['time'],:]
                if not sum_x.empty:
                    for index , row in sum_x.iterrows():
                        sum_jadid += sum(row.tolist())
            retSum[gostare_data[0][1]] = sum_jadid * 100
            n +=1
        x = 1

        # return x
        return [retSum , self.sum_percentage_pishraft(args['time'])]


    def opencsv(self):
        model_mali_csv = pandas.read_csv('model_mali2.csv')

        df = pandas.DataFrame(model_mali_csv)
        return df

    def makeDataFrame(self , inputs,time_monthe_and_year):
        i = 0
        n = 0
        time = []
        columns = []
        init = []
        data_fin = {}
        for input in inputs:

            data_fin[input] = []
            while i < 29:
                full_time = inputs[input][i][1]
                year_and_month = full_time.split('-')

                if year_and_month[0]+"-"+year_and_month[1] not in time:
                    full_time = inputs[input][i][1]
                    year_and_month = full_time.split('-')
                    if not year_and_month[0]+"-"+year_and_month[1] in time:
                        time.append(year_and_month[0]+"-"+year_and_month[1])
                init.append(inputs[input][i][0])
                data_fin[input].append(inputs[input][i][0])
                i += 1
            columns.append(str(input))
            i = 0
            # if n == 0:
            #     n = 1

        # print(data_fin)
        # print (time)
        #///////////////
        sum_ret = 0
        # return time_monthe_and_year.split('-')
        for input in inputs:
            for meghdar_har_ghest in inputs[input]:
                sum_ret += inputs[input][meghdar_har_ghest][0]
                # print (inputs[input][meghdar_har_ghest][0])
                if inputs[input][meghdar_har_ghest][1].split('-')[:2] == time_monthe_and_year.split('-'):
                    break
        for column in columns:
            print(len(data_fin[column]))
            if len(data_fin[column]) < len(time):
                # i = len(time) - len(data_fin[column])
                while len(data_fin[column])+1 <= len(time):
                    data_fin[column].append(0)
                    print(len(data_fin[column]))

        dataframe_init = pandas.DataFrame(index=time , data = data_fin,columns=columns)

        return dataframe_init

    def searchInDataframeToButtom(self,df , index):
        df = df.loc[str(index):]
        return df
    def searchInDataframeToTop(self,df , index):
        df = df.loc[:str(index)]
        return df
    def sumTheDataframe(self,df):

        sum = 0
        for index , data in df.iterrows():
            # print (data)
            for d in data:
                sum += d
        # print(sum)
        return sum

    def sum_percentage_pishraft(self,time):
        n = 1
        value = {}
        while n < 11:
            query = "select * from gostare_pishraft where tarikh <= %s and gostare_id = %s"
            values =(time,n)
            mycursor.execute(query,values)
            pishrafts = mycursor.fetchall()
            value[n]=0
            for pishraft in pishrafts:
                value[n] += float(pishraft[2])
            n +=1 
            mydb.commit()
        return value


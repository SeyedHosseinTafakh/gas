from DB import *
import os
import werkzeug
from flask import Flask , jsonify
from flask_restful import Api, Resource, reqparse
import pandas
import json
from timeFunctions import *
from dateutil.relativedelta import *
from datetime import timedelta
from khayyam import JalaliDate


class show_gostareha(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('gostare_id',required=True)
        parser.add_argument('tarikh',required=True)
        args = parser.parse_args()
        mycursor.execute("select * from gostare where id = %s ",(args['gostare_id'],))
        gostare_data = mycursor.fetchall()
        mydb.commit()
        #--------darsad tahaghogh yafte ta emrooz---------
        mycursor.execute("select * from gostare_pishraft where gostare_id = %s and tarikh <  %s",(gostare_data[0][0],args['tarikh']))
        pishrafts = mycursor.fetchall()
        mydb.commit()
        tajamoe_pishraft = 0
        for pishraft in pishrafts:
        	tajamoe_pishraft = tajamoe_pishraft +  float(pishraft[2])
        mablaghe_pardakhti_kol = [ 372573620,329359752 ,352718600, 393129406 ,40877983 ,167772070 , 167772070 , 151932166 , 151932166 , 151932167 ]
        mablaghe_pardakhti_ta_konoon = get_2(gostare_id= args['gostare_id'] , time=args['tarikh'])
        jarime = get_jarime(gostare_id=args['gostare_id'],tarikh=args['tarikh'])
        jarime_akhar = 0
        if 'an_ghozi' in jarime:
            jarime_akhar = jarime['an_ghozi'][1]
        for jarime_taki in jarime['jarime']:
        	jarime_akhar = jarime_akhar + jarime_taki[1]
        ret = {
        'name_gostare':gostare_data[0][1],
        'vazne_gostare_az_kol':gostare_data[0][2],
        'tarikh_barname_rizi_shode':gostare_data[0][3],
        'kare_anjam_gerefte':tajamoe_pishraft,
        'baghimande_pishraft':float(gostare_data[0][2]) - tajamoe_pishraft, 
        'mablaghe_pardakhti_kol':mablaghe_pardakhti_kol[gostare_data[0][0]-1],
        # 'mablaghe_pardakhti_kol':gostare_data[0][0]-1,
        'mablaghe_pardakhti_ta_konoon':mablaghe_pardakhti_ta_konoon,
        'jaraem_tajamoee':jarime_akhar
        }
        return ret

def get_2(gostare_id,time):
    # parser = reqparse.RequestParser()
    # parser.add_argument('gostare_id',required=True)
    # parser.add_argument('time',required=True)

    # args = parser.parse_args()
    args = {}
    args['gostare_id'] = gostare_id
    args['time'] = time


    mycursor.execute("select * from gostare where id = %s ",(args['gostare_id'],))
    gostare_data = mycursor.fetchall()
    mydb.commit()
    query = "select * from gostare_pishraft where gostare_id = %s and malg =1"
    values = (args['gostare_id'] ,)
    mycursor.execute(query , values)
    gostare_pishrafts = mycursor.fetchall()
    df = opencsv()
    # print (df)
    column = df.loc[0:29 , str(gostare_data[0][2]) + "%"]
    ret = {}
    i = 0
    for gostare_pishraft in gostare_pishrafts:
        ret[gostare_pishraft[5]] = {}

        d = gostare_pishraft[3].split('-')
        time = JalaliDate(d[0], d[1], d[2])
        month = int(d[1])
        while i < 29:
            if month > 12 :
                month = 1
                d[0] = str(int(d[0])+1)
            column[i]= column[i].replace(',', '')
            ret[gostare_pishraft[5]][i] = [
                float(gostare_pishraft[2])/float(gostare_data[0][2]) * float(column[i]),
                str(JalaliDate(d[0], str(month) , d[2]))
            ]
            i +=1
            month +=1
        i = 0
    dataFrame = makeDataFrame(ret)
    if args['time'] in dataFrame.index.tolist():
        dataFrame= searchInDataframeToTop(dataFrame,args['time'])
        sum = sumTheDataframe(dataFrame)
    else:
        sum = 0
    return sum

def opencsv():

    model_mali_csv = pandas.read_csv('model_mali.csv')
    df = pandas.DataFrame(model_mali_csv)
    return df

def makeDataFrame(inputs):
    i = 0
    n = 0
    time = []
    columns = []
    init = []
    data_fin = {}
    for input in inputs:

        data_fin[input] = []
        while i < 29:
            if inputs[input][i][1] not in time:
                time.append(inputs[input][i][1])
            init.append(inputs[input][i][0])
            data_fin[input].append(inputs[input][i][0])
            i += 1
        columns.append(str(input))
        i = 0
        # if n == 0:
        #     n = 1
    # print(data_fin)
    for input in inputs:
        i = len(data_fin[input])

        x = time.index(inputs[input][0][1])
        while n < x:
            data_fin[input].insert(0,0)
            n +=1
        n = 0
        x = time.index(inputs[input][28][1])
        x = abs(x - (len(time)-1))
        # print(x)
        while n <x:
            data_fin[input].append(0)
            n += 1
        n = 0

    dataframe_init = pandas.DataFrame(index=time , data = data_fin,columns=columns)
    # print (dataframe_init)
    return dataframe_init

def searchInDataframeToButtom(df , index):
    df = df.loc[str(index):]
    return df
def searchInDataframeToTop(df , index):
    df = df.loc[:str(index)]
    return df
def sumTheDataframe(df):
    # return True
    sum = 0
    for index , data in df.iterrows():
        # print(data)
       for d in data:
           sum += d

    return sum



def get_jarime(gostare_id,tarikh):

        #parser = reqparse.RequestParser()
        # parser.add_argument('gostare_id', required=True)
        #parser.add_argument('id_ghest')
        #parser.add_argument('date')
        #args = parser.parse_args()
        args = {}
        args['gostare_id'] = gostare_id
        args['date'] = tarikh
        mycursor.execute("select * from gostare where id = %s ", (args['gostare_id'],))
        gostare_data = mycursor.fetchall()
        mydb.commit()
        # if args['id_ghest']:
        #     query = "select * from gostare_pishraft where gostare_id = %s and id_ghest <= %s"
        #     values = (args['gostare_id'],args['id_ghest'])
        #     mycursor.execute(query, values)
        # else:
        query = "select * from gostare_pishraft where gostare_id = %s and tarikh < %s and malg2 = True"
        values = (args['gostare_id'],args['date'])
        mycursor.execute(query, values)
        gostare_pishrafts = mycursor.fetchall()
        # if args['id_ghest']:
        #     for gostare in gostare_pishrafts:
        #         if int(gostare[4]) <= int(args['id_ghest']):
        #             gostare_pishrafts.remove(gostare)
                    
        pishraft_kar = 0
        jadval_jarime = []
        darsad_tahaghogh_yafte = 0
        if len (gostare_pishrafts) > 0:
            for gostare in gostare_pishrafts:
                days_dif = khayam_type_days(gostare[3], gostare_data[0][3])
                jarime = (50000 * float(abs(days_dif)) * float(gostare[2]))
                jadval_jarime.append([gostare[5],jarime/100,gostare[3],gostare[2],gostare[4],gostare[7],gostare[0]])
                darsad_tahaghogh_yafte += float(gostare[2])
                pishraft_kar += float(gostare[2])
        # return pishraft_kar
        ret = {}
        if len(gostare_pishrafts) > 0:
            if args['date']:
                days_dif = khayam_type_days(gostare_data[0][3] , args['date'])
                jarime = (5000 * float(abs(days_dif))) * (float(pishraft_kar) - float(gostare_data[0][2]))
                ret['an_ghozi']=['کار باقی مانده تجمعی', abs(jarime/100), args['date'],abs(pishraft_kar - float(gostare_data[0][2])) ]

        jadval_koli = {
            'esme_gostare': gostare_data[0][1],
            'vazne_kole_khat': gostare_data[0][2],
            'darsad_tahaghogh_yafte':darsad_tahaghogh_yafte,
            'darsad_baghi_mande': float(gostare_data[0][2]) - darsad_tahaghogh_yafte,
            'tarikh':gostare_data[0][3]
        }
        ret['jadval_koli'] = jadval_koli
        ret['jarime'] = jadval_jarime
        # if args['id_ghest']:
        #     ret['jadval_dark2hasti'] = shandool(args['id_ghest'])
        # else:
        ret['jadval_darkhasti'] = None
        return ret






def shandool(id_ghest):
    # parser = reqparse.RequestParser()
    # parser.add_argument('id_ghest',required=True)
    # args = parser.parse_args()
    query = "select * from taahodat_pardakht_sherkat_naftanir where id_ghest ="+id_ghest
    mycursor.execute(query)
    naftanir = mycursor.fetchall()
    jarimeha = {}
    jarime = 0
    for i in naftanir:
        jarime += float(i[3])
    jarimeha['taahodat_pardakht_sherkat_naftanir'] = jarime
    jarime=0
    query = "select * from taahodat_pardakht_sherkat_mohandesi_tose_gas where id_ghest ="+id_ghest
    mycursor.execute(query)
    tose_gas = mycursor.fetchall()

    for i in tose_gas:
        jarime += float(i[3])
    jarimeha['taahodat_pardakht_sherkat_mohandesi_tose_gas'] = jarime
    jarime = 0
    query = "select * from jarime_taakhir_dar_pardakht where id_ghest ="+id_ghest
    mycursor.execute(query)
    jarime_taakhir_dar_pardakht = mycursor.fetchall()
    mydb.commit()

    for i in jarime_taakhir_dar_pardakht:
        jarime += float(i[2])
    jarimeha['jarime_taakhir_dar_pardakht'] = jarime

    return jarimeha











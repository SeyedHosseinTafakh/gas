from DB import *
import os
import werkzeug
from flask import Flask , jsonify,send_from_directory
from flask_restful import Api, Resource, reqparse
import random



class download(Resource):
	def get(self):
		dirname = os.path.dirname(__file__)
		parser = reqparse.RequestParser()
		parser.add_argument('file_name',required=True)
		args = parser.parse_args()
		if not os.path.isfile(os.path.join(dirname,'../files/'+args['file_name'])):
			return 'file not found'
		return send_from_directory(os.path.join(dirname,'../files'),filename=args['file_name'])
		


from DB import *
from decimal import *
import os
import werkzeug
from flask import Flask, jsonify

from flask_restful import Api, Resource, reqparse
from khayyam import *
from datetime import timedelta
from timeFunctions import khayam_type_days


class add_motalebat_tarazmali_to_taahodat_pardakht(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("nerkh", required=True)
        parser.add_argument("mablagh", required=True)
        args = parser.parse_args()
        fileName = None

        a = float(args['mablagh'])
        b = float(args['nerkh'])
        mablagh_dollar = a / b
        mycursor.execute("SELECT id FROM taahodat_pardakht_sherkat_naftanir where id = 999")
        result = mycursor.fetchall()
        if result == []:
            sql = "INSERT INTO taahodat_pardakht_sherkat_naftanir (id,tarikh , sharh,mablagh_dollari,tozihat,file_peyvast,id_ghest) VALUES (%s,%s,%s,%s,%s,%s ,%s)"
            values = (
            "999", "1396-09-17", "مطالبات تراز مالی ریالی", mablagh_dollar, "نرخ دلار " + args['nerkh'], fileName, "1")
            mycursor.execute(sql, values)
            mydb.commit()
        else:
            sql = "UPDATE taahodat_pardakht_sherkat_naftanir SET mablagh_dollari=%s, tozihat=%s where id = 999"
            val = (mablagh_dollar, "نرخ دلار " + args['nerkh'])
            mycursor.execute(sql, val)
            mydb.commit()

        return True


class add_motalebat_tarazmali_to_taahodat_pardakht_dolar(Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("mablagh", required=True)
        args = parser.parse_args()
        fileName = None

        a = float(args['mablagh'])
        b = 1
        mablagh_dollar = a / b
        mycursor.execute("SELECT id FROM taahodat_pardakht_sherkat_naftanir where id = 998")
        result = mycursor.fetchall()
        if result == []:
            sql = "INSERT INTO taahodat_pardakht_sherkat_naftanir (id,tarikh , sharh,mablagh_dollari,tozihat,file_peyvast,id_ghest) VALUES (%s,%s,%s,%s,%s,%s ,%s)"
            values = ("998", "1396-09-17", "مطالبات تراز مالی دلاری", mablagh_dollar, "نرخ دلار  1", fileName, "1")
            mycursor.execute(sql, values)
            mydb.commit()
        else:
            sql = "UPDATE taahodat_pardakht_sherkat_naftanir SET mablagh_dollari=%s, tozihat=%s where id = 998"
            val = (mablagh_dollar, "نرخ دلار 1")
            mycursor.execute(sql, val)
            mydb.commit()

        return True
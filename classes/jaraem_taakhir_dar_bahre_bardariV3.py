from DB import *
import os
import werkzeug
from flask import Flask , jsonify
from flask_restful import Api, Resource, reqparse
from khayyam import *
from datetime import timedelta
from timeFunctions import khayam_type_days
from classes.jaraem_taakhir_dar_bahre_bardariV2 import *

class jaraem_taakhir_dar_bahre_bardariV3(Resource):
    def get(self):
        dates_base_on_id_ghest = [
    ["1396-09","1","یک - آذر96"],
    ["1396-10","2","دوم - دی 96"],
    ["1396-11","3","سوم - بهمن 96"],
    ["1396-12","4","چهارم - اسفند 96"],
    ["1397-01","5","پنجم - فروردین97"],
    ["1397-02","6","ششم - اردیبهشت97"],
    ["1397-03","7","هفتم - خرداد97"],
    ["1397-04","8","هشتم - تیر97"],
    ["1397-05","9","نهم - مرداد97"],
    ["1397-06","10","دهم - شهریور97"],
    ["1397-07","11","یازدهم - مهر97"],
    ["1397-08","12","دوازدهم - آبان97"],
    ["1397-09","13","سیزدهم - آذر97"],
    ["1397-10","14","چهاردهم - دی97"],
    ["1397-11","15","پانزدهم - بهمن97"],
    ["1397-12","16","شانزدهم - اسفند97"],
    ["1398-01","17","هفدهم - فروردین 98"],
    ["1398-02","18","هجدم - اردیبهشت 98"],
    ["1398-03","19","نوزدهم - خرداد 98"],
    ["1398-04","20","بیستم - تیر 98"],
    ["1398-05","21","بیست و یکم - مرداد 98"],
    ["1398-06","22","بیست و دوم -شهریور 98"],
    ["1398-07","23","بیست و سوم - مهر 98"],
    ["1398-08","24","بیست و چهارم - آبان 98"],
    ["1398-09","25","بیست و پنچم - آذر 98"],
    ["1398-10","26","بیست و ششم - دی 98"],
    ["1398-11","27","بیست و هفتم - بهمن 98"],
    ["1398-12","28","بیست و هشتم - اسفند 98"],
    ["1399-01","29","بیست و نهم - فروردین99"],
    ["1399-02","30","سی ام - اردیبهشت99"],
    ["1399-03","31","سی و یکم - خرداد99"],
    ["1399-04","32","سی و دوم - تیر99"],
    ["1399-05","33","سی و سوم - مرداد99"],
    ["1399-06","34","سی و چهارم - شهریور99"],
    ["1399-07","35","سی و پنچم - مهر99"],
    ["1399-08","36","سی و ششم - آبان99"],
    ["1399-09","37","سی و هفتم - آذر99"],
    ["1399-10","38","سی و هشتم - دی99"],
    ["1399-11","39","سی و نهم - بهمن99"],
    ["1399-12","40","چهلم - فروردین1400"],
    ["1400-01","41","چهل و یک - اردیبهشت1400"],
    ["1400-02","42","چهل و دو - خرداد1400"]
  ]
        parser = reqparse.RequestParser()
        parser.add_argument('id_ghest',required=True)
        # parser.add_argument('date',required=True)
        args = parser.parse_args()
        date = dates_base_on_id_ghest[int(args['id_ghest'])-1][0]+'-17'

        n = 1
        jarime_har_gostare = []
        ret = {}
        while n <= 10:
            jarime_har_gostare.append(get_gostare_id(n ,args['id_ghest'],date))
            # return jarime_har_gostare
            # print ( n)
            n +=1
        # return  jarime_har_gostare
        for jarime in jarime_har_gostare:
            # print(n)
            # print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
            if 'an_ghozi' in jarime:
                jarime_anghozie = jarime['an_ghozi'][1] / 10
                print(jarime_anghozie)
            else:
                jarime_anghozie = 0
            # return jarime['an_ghozi'][1]
            ret[jarime['jadval_koli']['esme_gostare']] = 0
            if 'jarime' in jarime:
                for tak_jarime in jarime['jarime']:
                    # print(tak_jarime)
                    ret[jarime['jadval_koli']['esme_gostare']] += tak_jarime[1]
                    print(ret[jarime['jadval_koli']['esme_gostare']])
                ret[jarime['jadval_koli']['esme_gostare']]  += jarime_anghozie
        return ret






def shandool(id_ghest):
    # parser = reqparse.RequestParser()
    # parser.add_argument('id_ghest',required=True)
    # args = parser.parse_args()
    query = "select * from taahodat_pardakht_sherkat_naftanir where id_ghest <="+id_ghest
    mycursor.execute(query)
    naftanir = mycursor.fetchall()
    jarimeha = {}
    jarime = 0
    for i in naftanir:
        jarime += float(i[3])
    jarimeha['taahodat_pardakht_sherkat_naftanir'] = jarime
    jarime=0
    query = "select * from taahodat_pardakht_sherkat_mohandesi_tose_gas where id_ghest <="+id_ghest
    mycursor.execute(query)
    tose_gas = mycursor.fetchall()

    for i in tose_gas:
        jarime += float(i[3])
    jarimeha['taahodat_pardakht_sherkat_mohandesi_tose_gas'] = jarime
    jarime = 0
    query = "select * from jarime_taakhir_dar_pardakht where id_ghest <="+id_ghest
    mycursor.execute(query)
    jarime_taakhir_dar_pardakht = mycursor.fetchall()
    mydb.commit()

    for i in jarime_taakhir_dar_pardakht:
        jarime += float(i[2])
    jarimeha['jarime_taakhir_dar_pardakht'] = jarime

    return jarimeha



def get_gostare_id(gostare_id , id_ghest , date):


    # parser = reqparse.RequestParser()
    # parser.add_argument('gostare_id', required=True)
    # parser.add_argument('id_ghest')
    # parser.add_argument('date')
    # args = parser.parse_args()
    args={}
    args['gostare_id']  = gostare_id
    args['id_ghest'] = id_ghest
    args['date'] = date
    mycursor.execute("select * from gostare where id = %s ", (args['gostare_id'],))
    gostare_data = mycursor.fetchall()
    mydb.commit()
    if args['id_ghest']:
        query = "select * from gostare_pishraft where gostare_id = %s and id_ghest <=%s"
        values = (args['gostare_id'],args['id_ghest'])
        mycursor.execute(query, values)
    else:
        query = "select * from gostare_pishraft where gostare_id = %s "
        values = (args['gostare_id'],)
        mycursor.execute(query, values)
    gostare_pishrafts = mycursor.fetchall()
    pishraft_kar = 0
    jadval_jarime = []
    darsad_tahaghogh_yafte = 0
    if len (gostare_pishrafts) > 0:
        for gostare in gostare_pishrafts:
            days_dif = khayam_type_days(gostare[3], gostare_data[0][3])
            jarime = (50000 * float(abs(days_dif)) * float(gostare[2]))
            jadval_jarime.append([gostare[5],jarime/100,gostare[3],gostare[2],gostare[4],gostare[7],gostare[0]])
            darsad_tahaghogh_yafte += float(gostare[2])
            pishraft_kar += float(gostare[2])
    # return pishraft_kar
    ret = {}
    if len(gostare_pishrafts) > 0:
        if args['date']:
            print('spliter')
            days_dif = khayam_type_days(gostare_data[0][3] , args['date'])
            jarime = (5000 * float(abs(days_dif))) * (float(pishraft_kar) - float(gostare_data[0][2]))
            ret['an_ghozi']=['کار باقی مانده تجمعی', abs(jarime), args['date'],abs(pishraft_kar - float(gostare_data[0][2])) ]

    jadval_koli = {
        'esme_gostare': gostare_data[0][1],
        'vazne_kole_khat': gostare_data[0][2],
        'darsad_tahaghogh_yafte':darsad_tahaghogh_yafte,
        'darsad_baghi_mande': float(gostare_data[0][2]) - darsad_tahaghogh_yafte,
        'tarikh':gostare_data[0][3]
    }
    ret['jadval_koli'] = jadval_koli
    ret['jarime'] = jadval_jarime
    if args['id_ghest']:
        ret['jadval_dark2hasti'] = shandool(args['id_ghest'])
    else:
        ret['jadval_darkhasti'] = None
    return ret
